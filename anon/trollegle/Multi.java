package anon.trollegle;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.AbstractCollection;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import static anon.trollegle.UserConnection.JoinType;
import static anon.trollegle.Util.addMapping;
import static anon.trollegle.Util.clearMappings;
import static anon.trollegle.Util.randomize;
import static anon.trollegle.Util.removeMapping;
import static anon.trollegle.Util.t;
import static anon.trollegle.Util.tRandom;
import static anon.trollegle.Util.tRandomLine;

/**
 * Manages a group chat of MultiUsers.
 */
public class Multi implements Callback<MultiUser>, Proxies.Listener {

    /** Users apart from the dummy */
    List<MultiUser> users = new ArrayList<>();
    /** Dummy user attached to the console */
    private MultiUser consoleDummy;
    /** Names of files with commands to load on startup */
    private List<String> rcFiles = new ArrayList<>();
    /** Names of JSON files with serialized state to load on startup */
    private List<String> jsonFiles = new ArrayList<>();
    /** Whether a shutdown hook was added. Used to save serialized state when accidentally interrupted. */
    private boolean shutdownHookAdded;
    /** Time of last save to JSON. If less than SAVE_GRACE ago, the shutdown hook doesn't save once more. */
    protected long lastSave;
    protected final long SAVE_GRACE = 20000;
    
    /** Proxy manager */
    protected final Proxies proxies = makeProxies();
    /** Hook for creating the proxy manager. */
    protected Proxies makeProxies() { return new Proxies(this); }
    /** Whether new users will be moved back to the direct connection after starting the session with a proxy */
    @Persistent
    protected boolean moveProxies = false;
    
    /** Commands specific to admins */
    private AdminCommands commands = makeAdminCommands();
    /** Normal user commands */
    private UserBehaviour userBehaviour = makeUserBehaviour();
    /** Commands for users that declare themselves as bots */
    private BotBehaviour botBehaviour = makeBotBehaviour();
    // Hooks to add custom commands
    protected AdminCommands makeAdminCommands() { return new AdminCommands(this); }
    protected UserBehaviour makeUserBehaviour() { return new UserBehaviour(this); }
    protected BotBehaviour makeBotBehaviour() { return new BotBehaviour(this); }
    
    /** Virtual collection containing both the console dummy and the normal users */
    protected Collection<MultiUser> allUsers = new AbstractCollection<MultiUser>() {
        public int size() {
            return consoleDummy == null ? users.size() : 1 + users.size();
        }
        public Iterator<MultiUser> iterator() {
            return new Iterator<MultiUser>() {
                Iterator<MultiUser> back =
                    consoleDummy == null ? users.iterator() : null;
                public MultiUser next() {
                    if (back != null)
                        return back.next();
                    back = users.iterator();
                    return consoleDummy;
                }
                public boolean hasNext() {
                    return back == null || back.hasNext();
                }
            };
        }
    };

    /** Map of which users have voted to kick which users. Commands: /kick, /nokick */
    private Map<MultiUser, Set<MultiUser>> kickVotes = new WeakHashMap<>();
    /** Map of which users ignore which users. Commands: /ignore, /unignore */
    private Map<MultiUser, Set<MultiUser>> ignored = new WeakHashMap<>();

    /**
     * Patterns such that if a message matches them, it is suppressed or the user is muted or kicked.
     * Each ban includes validity parameters (time and message count) and the action to take.
     * Commands: /!ban, /!checkban, /!clearbans
     */
    @Persistent
    private Ban[] bans = {};
    /** Lock for writing to bans */
    private final Object banLock = new Object();
    /**
     * Likelihood that matching an unconfigured ban will result in a mute rather than a kick.
     * Command: /!banmute
     */
    @Persistent
    double muteBotFreq = 0.9;
    /** Number of messages after a user joins for which an unconfigured ban will match. Command: /!banspan */
    @Persistent
    int banSpan = 60;
    /** Time after a user joins (-1: forever) for which an unconfigured ban will match. Command: /!banspantime */
    @Persistent
    long banSpanTime = -1;

    /** Frequency of adding new users from Omegle at large, when this is enabled (/spam) */
    @Persistent
    long joinFreq = 3000, // Part multiplied by the number of users present. Command: /!freq
        joinFreqConstant = 4000; // Constant part of frequency. Command: /!freqconst
    /** Frequency of renewing the pulse (Omegle session that waits for returning users. Command: /!pfreq */
    @Persistent
    long pulseFreq = 700000;
    /**
     * User limits per proxy.
     * If the number of users in the room divided by the number of proxies is over maxChatting, or the 
     * total number of sessions is over maxTotal, no new users will be added.
     */
    @Persistent
    int maxChatting = 9, maxTotal = 11;
    
    /**
     * Numeric IDs for users.
     * Console user is ID 0. New users are assigned the lowest ID that isn't currently used and wasn't
     * used for the last user to leave.
     */
    private MultiUser[] ids = new MultiUser[maxTotal * 3 / 2];
    @Persistent
    private int lastFreedId = -1;
    
    /**
     * Flooding kick: if a user sends more than floodKickSize messages within floodKickTime ms.
     * Command: /!flood
     */
    @Persistent
    long floodKickTime = 7500;
    @Persistent
    int floodKickSize = 4;
    /**
     * More restrictive, but non-kicking, flood limit for new joins: one message every
     * floodControlTime ms for a user's first floodControlSpan messages.
     */
    @Persistent
    long floodControlTime = 3500; // Command: /!floodcontrol
    @Persistent
    int floodControlSpan = 9; // Command: /!floodcontrolspan
    /** Timeouts relating to kicking */
    @Persistent
    private long inactiveBot = 13 * 30 * 1000, // Inactivity kick time for users not in the room
        inactiveHuman = 13 * 60 * 1000, // Inactivity kick time for users in the room
        lurkingHuman = 3 * 60 * 1000, // Inactivity after which a user is ignored when counting kick votes
        matureHuman = 90000; // Minimum age for casting kick votes. Command: /!maturehuman
    /** Status of temporarily pausing the pulses. Commands: /!pausepulse, /!resumepulse */
    @Persistent
    private long
        disablePulseTime, // Time at which pulse was paused
        pulseCooldownTime; // Duration after which pulse will resume
    /** Whether new users will be gathered from Omegle at large. Commands: /spam, /nospam */
    @Persistent
    private boolean autoJoin;
    /**
     * Probabilities for gathering new users per Omegle mode. 
     * 1 - qfreq - bareFreq is the probability that interest mode will be used.
     */
    @Persistent
    double qfreq = 0, // Probability that question mode will be used. Command: /!qfreq
        bareFreq = 0.02, // Probability that text mode (no interests) will be used. Command: /!barefreq
        topicSample = 1d/3;
    /**
     * Topics that will be used by default when adding a user from interest mode.
     * Commands: /!topics, /!addtopics, /!removetopics
     * These can be overridden by the topics parameter of /!invitec
     */
    @Persistent
    private String[] defaultTopics = {
            "groupchat", "irc", "groupchats", "chatrooms", "math", "maths", "language", "programming", "government"};
    /** Time of last session opened to add a new user */
    @Persistent
    private long lastInvite = System.currentTimeMillis();
    /** Time of last session opened to add a returning user */
    @Persistent
    private long lastPulse = System.currentTimeMillis();
    /** Exponent to current join frequency for limiting /invite flooding */
    @Persistent
    private double minIntervalExponent = 2d/3;
    /** If false, spamming is automatically enabled when the last user leaves. */
    @Persistent
    boolean murder;
    @Persistent
    boolean countMuted = true, showLurkChanges = false;
    @Persistent
    String challenge = Util.makeRandid() + Util.makeRandid();
    @Persistent
    String password = Util.makeRandid() + Util.makeRandid();
    Object adminToken = new Object();
    @Persistent
    private String chatName = "(none)";
    /** Minimum time between pats. Command: /pat */
    @Persistent
    private long patCooldown = 5000;

    /** Short-term message history for duplicate suppression */
    protected static class Message {
        MultiUser user;
        String text;
    }
    private Message[] history = {};
    private final Object historyLock = new Object();
    @Persistent
    private int historyPosition;
    
    /** Join types for which to require an entry word */
    @Persistent
    EnumSet<JoinType> toothpasteTypes = EnumSet.of(JoinType.TEXT, JoinType.BARE);
    
    @Persistent
    private Pattern toothpastePattern = Pattern.compile(t("toothpaste"));
    @Persistent
    private String toothpastePrompt = t("toothpasteprompt");
    
    /**
     * Creating a Multi starts a thread with an infinite loop that takes care of:
     * - adding users
     * - removing inactive users
     * - managing lurker status
     * - switching proxies
     */
    public Multi() {
        Util.makeThread(() -> {
            while (true) {
                long loopFreq = Math.min(250, joinFreq * Math.max(spamSize() - 1, 1));
                Util.sleep(loopFreq / 2 + (long) (Math.random() * loopFreq));
                mainLoop(false);
            }
        }).start();
    }
    
    // Following three shouldn't be accessed outside of mainLoop!
    /** Users to kick for inactivity */
    private ArrayDeque<MultiUser> kickTargets = new ArrayDeque<>();
    /** Counts for treating proxies as banned due to zombie connections */
    private HashMap<ProxyConfig, int[]> zombies = new HashMap<>();
    /** Whether the last run of the main loop was triggered by an event, rather than timed */
    @Persistent
    private boolean lastWasTriggered;
    /**
     * Main loop. This is run on a timer as well as on certain events, such as dead pulses.
     * 1) Proxy is switched if the current proxy has been active for more the proxyLife (see Proxies)
     * 2) Users for inactivity kicking are gathered, zombies are counted and lurk status is updated
     * 3) Inactive users are kicked
     * 4) Pulse is renewed if it has expired
     * 5) Tor searches are started for instances that are unusable
     */
    protected synchronized void mainLoop(boolean triggered) {
        if (lastWasTriggered && triggered)
            return;
        lastWasTriggered = triggered;
        try {
            if ((autoJoin || pulseFreq > -1) && !proxies.isBanned())
                proxies.switchIfStale();

            int pulses = 0;
            zombies.clear();
            final HashSet<ProxyConfig> startedProxies = new HashSet<>();
            final HashSet<ProxyConfig> usedProxies = new HashSet<>();
            final List<MultiUser> unpulseTargets = new ArrayList<>();

            synchronized (users) {
                for (MultiUser u : users) {
                    if (u.isPulse()) {
                        if (u.idleFor() > pulseFreq || getPulseCooldown() > 100) {
                            unpulseTargets.add(u);
                        } else {
                            pulses++;
                        }
                        continue;
                    }
                    if ((!u.isReady() || u.isMuted()) && u.idleFor() > inactiveBot
                            || u.idleFor() > inactiveHuman * (!shouldRevive() ? 3 : 1)) {
                        kickTargets.add(u);
                    } else if (u.shouldInformLurker()) {
                        tellRoom(to -> behave(to).updateLurk(to, u));
                    }
                    if (u.idleFor() > inactiveBot && !u.isConnected() && !u.isPulseEver()) {
                        int[] count = zombies.get(u.getProxy());
                        if (count == null) {
                            zombies.put(u.getProxy(), new int[] {1});
                        } else {
                            count[0]++;
                        }
                    } else if (u.isConnected()) {
                        startedProxies.add(u.getProxy());
                        if (u.isUsingProxy())
                            usedProxies.add(u.getProxy());
                    }
                }
            }
            while (!kickTargets.isEmpty())
                kickInactive(kickTargets.pop());
            
            Util.forEachCompromise(unpulseTargets, u -> u.unpulse(shouldConvertPulse()));
            
            for (Map.Entry<ProxyConfig, int[]> entry : zombies.entrySet())
                if (entry.getValue()[0] > 1)
                    banned(entry.getKey(), "zombie users");

            if (autoJoin && System.currentTimeMillis() - lastInvite >= joinFreq() && !proxies.isBanned()
                    && spamSize() < maxChatting() && users.size() < maxTotal() && !proxies.isDirty()) {
                add();
                lastInvite = System.currentTimeMillis();
            }
            
            if (pulses == 0)
                notifyPulseResumed();
            if (pulses == 0 && shouldAddPulse()) {
                if (!proxies.isDirty() || proxies.switchProxy())
                    if (addPulse())
                        lastPulse = System.currentTimeMillis();
            }
            
            if (System.currentTimeMillis() - lastSave > SAVE_GRACE) {
                StringBuilder started = proxies.startTorSearches(usedProxies);
                proxies.pingStatus(startedProxies);
                
                if (started.length() > 0)
                    tellAdmin("Starting Tor search for: " + started.substring(2));
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /** Whether an expired pulse should be converted into a bare (no interests) connection. */
    protected boolean shouldConvertPulse() {
        return autoJoin && bareFreq > 0;
    }
    
    protected boolean shouldAddPulse() {
        return pulseFreq >= 0 
            && getPulseCooldown() == 0
            && System.currentTimeMillis() - lastPulse >= pulseFreq
            && !proxies.isBanned()
            && users.size() < maxTotal();
    }
    
    /** Only here for overriding. Use makeIdUser everywhere else. */
    protected MultiUser makeUser() {
        return new MultiUser(this);
    }
    
    /** Makes a user using makeUser() and assigns it an ID */
    protected MultiUser makeIdUser() {
        synchronized (users) {
            resizeIds();
            int id = freeId();
            ids[id] = makeUser().withNumber(id);
            ids[id].setMoveProxy(moveProxies);
            return ids[id];
        }
    }
    
    /** Ends the chat: disables addition of new users and kicks all current users */
    public void massacre() {
        murder = true;
        autoJoin = false;
        pulseFreq = -1;
        tellRoom(t("chatover"));
        Util.sleep(1500);

        ArrayList<MultiUser> toKick;
        synchronized (users) {
            toKick = new ArrayList<>(users);
        }
        for (MultiUser u : toKick) {
            u.schedSendDisconnect();
            u.dispose();
            remove(u);
            Util.sleep((int) (Math.random() * joinFreq));
        }
        System.out.println();
    }
    
    /** Returns the maximum connections to open, scaled for the number of proxies */
    public int maxTotal() {
        return maxTotal * proxies.count();
    }
    
    /** Returns the maximum number of users in the room, scaled for the number of proxies */
    public int maxChatting() {
        return maxChatting * (proxies.count() + 1) * 2 / 3;
    }
    
    /** Returns whether adding new users (spamming) is possible and enabled */
    public boolean effectiveAutoJoin() {
        return autoJoin && !proxies.isBanned();
    }
    
    /** Returns whether adding new users (spamming) should be reactivated after the last user leaves */
    public boolean shouldRevive() {
        return !murder && !proxies.isBanned();
    }
    
    /** Prints the frequencies at which users were being added. Called after a ban from Omegle. */
    protected String showFreqs() {
        return "spam freq: " + joinFreqConstant + "+" + joinFreq + "*n^2, pulse: " + pulseFreq;
    }
    
    /** Handles a captcha ban detected in the given session. */
    public void captchad(final MultiUser source, String challenge) {
        tellAdmin("Captcha ban for " + source.getProxy() + ". " + showFreqs());
        if (proxies.handleCaptcha(source.getProxy())) {
            mainLoop(true);
            return;
        }
        if (proxies.shouldShowBans()) {
            tellRoom(t("captcha"));
        }

    }

    /** Handles a non-captcha ban detected in the given session. */
    public void banned(ProxyConfig proxy, String type) {
        if (type.startsWith("empty")) {
            tellAdmin("*Possible* ban for " + proxy + ": " + type + ". " + showFreqs());
            proxy.dirty();
            if (proxies.switchProxy())
                mainLoop(true);
            return;
        }
        tellAdmin("Ban for " + proxy + ": " + type + ". " + showFreqs());
        if (proxies.handleBan(proxy)) {
            mainLoop(true);
            return;
        }
        if (proxies.shouldShowBans())
            tellRoom(t("hardban"));
    }

    /**
     * Adds a ban expression expressed as a string.
     * If a ban with the same regex exists, it is replaced with this one.
     * See Ban for details on syntax. Command: /!ban
     */
    public void ban(String expr) {
        Ban ban;
        try {
            ban = new Ban(expr);
        } catch (PatternSyntaxException e) {
            tellAdmin("Syntax error: " + e.getMessage());
            return;
        }
        ban(ban);
    }
    /** Adds a ban expression */
    public void ban(Ban ban) {
        String canonical = ban.toString();
        synchronized (banLock) {
            Ban[] oldBans = bans;
            for (int i = 0; i < oldBans.length; i++) {
                if (ban.pattern.equals(oldBans[i].pattern)) {
                    if (canonical.equals(oldBans[i].toString()))
                        return;
                    Ban[] newBans = oldBans.clone();
                    newBans[i] = ban;
                    bans = newBans;
                    return;
                }
            }
            Ban[] newBans = Arrays.copyOf(oldBans, oldBans.length + 1);
            newBans[oldBans.length] = ban;
            bans = newBans;
        }
    }
    
    /** Removes a ban expression by regex. Command: /!unban */
    public void unban(String expr) {
        Ban ban;
        try {
            ban = new Ban(expr);
        } catch (PatternSyntaxException e) {
            tellAdmin("Syntax error: " + e.getMessage());
            return;
        }
        unban(ban);
    }
    public void unban(Ban ban) {
        Predicate<Ban> shouldKeep = other -> !ban.pattern.equals(other.pattern);
        synchronized (banLock) {
            bans = Util.filter(bans, shouldKeep);
        }
    }
    
    /** Returns all ban expressions */
    public Ban[] getBans() {
        return bans.clone();
    }
    
    /** Removes all disabled ban expressions. Command: /!prunebans */
    public void pruneBans() {
        synchronized (banLock) {
            bans = Util.filter(bans, Ban::isEnabled);
        }
    }

    /** Removes all ban expressions. Command: /!clearbans */
    public void clearBans() {
        synchronized (banLock) {
            bans = new Ban[0];
        }
    }
    
    /** Returns the number of active users, as used to calculate the spam frequency */
    public int spamSize() {
        int size = 0;
        synchronized (users) {
            for (MultiUser u : users)
                if (u.isReady() && !u.isMuted() && !u.isLurker())
                    size++;
        }
        return size;
    }
    
    /** Returns the number of active users for the /list command */
    public int listSize(boolean muted, boolean lurker) {
        int size = 0;
        synchronized (users) {
            for (MultiUser u : users)
                if (u.isReady() && (muted || countMuted || !u.isMuted())
                        && (lurker || !u.isLurker()))
                    size++;
        }
        return size;
    }
    
    /** Returns whether the topic should be listed in the /list shown to the target */
    public boolean showInList(MultiUser target, MultiUser topic) {
        if (!topic.isReady() || topic.isLurker() && !target.isLurker())
            return false;
        return countMuted || !topic.isMuted() || target.isMuted();
    }

    /**
     * Returns the effective frequency at which new users will be added, based on 
     * - joinFreqConstant (command: /!freqconst)
     * - joinFreq (command: /!freq)
     * - the number of active users as counted by spamSize()
     */
    public long joinFreq() {
        int spamSize = spamSize();
        return joinFreqConstant + (spamSize == 0 ? joinFreq : joinFreq * spamSize * spamSize);
    }
    
    /** Returns the minimum time between manually inviting users */
    public long minInterval() {
        return (long) Math.pow(joinFreq(), minIntervalExponent);
    }

    /** Returns a user by their name or numeric ID, or null if no such user */
    public MultiUser userFromName(String name) {
        if (name.matches("[0-9]+"))
            try {
                synchronized (users) {
                    int number = Integer.parseInt(name);
                    if (number >= ids.length)
                        return null;
                    return ids[number];
                }
            } catch (NumberFormatException e) {
                return null;
            }
        else
            synchronized (users) {
                return allUsers.stream()
                    .filter(a -> name.equals(a.getNick()))
                    .findFirst().orElse(allUsers.stream()
                        .filter(a -> name.equalsIgnoreCase(a.getNick()))
                        .findFirst().orElse(null));
            }
    }
    
    /** Returns how the topic's name should be displayed to the target */
    protected String getDisplayNick(MultiUser target, MultiUser topic) {
        return target.getMessageStyle().formatName(topic);
    }

    /** Returns how the topic's name, interests and age should be displayed to the target */
    protected String getDisplayTrivia(MultiUser target, MultiUser topic) {
        return target.getMessageStyle().formatTrivia(topic);
    }
    
    /**
     * Relays a simple message from a user to the room, formatting it with the given template.
     * This is for messages typed without a command and /me messages.
     */
    public void relay(String template, MultiUser from, String message) {
        relay(template, from, message, null, null);
    }
    
    /**
     * Relays a message from or about a user to the room, filtered by the given filter.
     * type_ is an extra parameter to some templates.
     * extraFilter narrows the filtering done by shouldSendBasic() and replaces that done by shouldSendDefault().
     */
    public void relay(String template_, MultiUser from, String message, String type_, Predicate<MultiUser> extraFilter) {
        if (extraFilter == null)
            from.recordMessage();
        boolean normal = "normal".equals(template_) || "special".equals(template_);
        String template = template_, type = type_;
        Predicate<MultiUser> filter = to -> 
            shouldSendBasic(normal, message, from, to)
            && (extraFilter != null || shouldSendDefault(normal, message, from, to));
        if (extraFilter != null)
            filter = filter.and(extraFilter);
        tellRoom(u -> u.schedSend(t(template, getDisplayNick(u, from), message, type)), filter);
    }
    
    /**
     * The filter always applied by relay().
     * Non-command messages from oneself, and messages from ignored users, are filtered out.
     */
    protected boolean shouldSendBasic(boolean normal, String message, MultiUser from, MultiUser to) {
        return !(normal && to == from && from != consoleDummy) && !ignores(to, from);
    }
    
    /**
     * The filter applied by reply() when no other is given.
     * Messages from muted to non-muted users, and from lurkers to non-lurker non-admins, are filtered out.
     */
    protected boolean shouldSendDefault(boolean normal, String message, MultiUser from, MultiUser to) {
        return (!from.isMuted() || to.isMuted())
            && (isAdmin(to) || to.isLurker() || !from.isLurker()) 
            && !(!from.isMuted() && to.isMuted() && message.matches("(?i).*mute.*"));
    }
    
    /** Records a message in the history and returns true if it is a duplicate */
    protected boolean isDuplicate(MultiUser user, String text) {
        text = canonicalString(text);
        synchronized (historyLock) {
            if (history.length == 0)
                return false;
            for (Message message : history)
                if (duplicateEquals(message, user, text))
                    return true;
            history[historyPosition].user = user;
            history[historyPosition].text = text;
            historyPosition++;
            historyPosition %= history.length;
            return false;
        }
    }
    
    /** Criteria for considering two messages as duplicates */
    protected boolean duplicateEquals(Message message, MultiUser user, String text) {
        if (message.user == null || message.text == null)
            return false;
        if (user != message.user && 
                (user.getMsgCount() < 2 || matureHuman >= 0 && user.age() > matureHuman))
            return false;
        return message.text.equals(text);
    }
    
    @Persistent
    private Pattern spaces = Pattern.compile("(?uUs)\\s+");
    @Persistent
    private Pattern invisible = Pattern.compile("(?uUs)[^\\w\\s]+");
    
    /** Quick and dirty string normalization for isDuplicate() */
    protected String canonicalString(String message) {
        message = message.trim();
        message = spaces.matcher(message).replaceAll(" ");
        message = invisible.matcher(message).replaceAll("");
        message = message.toLowerCase(Locale.ROOT);
        return message;
    }
    
    /** Sets the size of the history for duplicate detection. Command: /!historysize */
    public void setHistorySize(int size) {
        synchronized (historyLock) {
            if (size <= 0) {
                history = new Message[0];
            } else if (size < history.length) {
                int start = Math.max(0, historyPosition - size);
                history = Arrays.copyOfRange(history, start, start + size);
                historyPosition -= start;
                historyPosition %= history.length;
            } else if (size > history.length) {
                historyPosition = history.length;
                history = Arrays.copyOf(history, size);
                for (int i = historyPosition; i < size; i++)
                    history[i] = new Message();
            }
        }
    }
    
    public int getHistorySize() { return history.length; }
    public long getMatureHuman() { return matureHuman; }
    public void setMatureHuman(long matureHuman) { this.matureHuman = matureHuman; }
    
    /** Sends the message using the system template to all lurkers */
    public void tellLurkers(String message) {
        tellRoom(u -> u.schedTell(message), u -> u.isLurker());
    }

    /** Sends the message using the system template to all muted users */
    public void tellMuted(String message) {
        tellRoom(u -> u.schedTell(message), u -> u.isMuted());
    }
    
    /** Sends the message using the system template to all users in the room */
    public void tellRoom(String message) {
        tellRoom(u -> u.schedTell(message));
    }
    
    /** Runs the action on all users in the room */
    public void tellRoom(Consumer<MultiUser> message) {
        tellRoom(message, null);
    }

    /** Runs the action on all users satisfying the filter */
    public void tellRoom(Consumer<MultiUser> message, Predicate<MultiUser> filter) {
        synchronized (users) {
            if (filter != null)
                allUsers.stream().filter(UserConnection::isReady).filter(filter).forEach(message);
            else
                allUsers.stream().filter(UserConnection::isReady).forEach(message);
        }
    }
    
    /**
     * Sends the message, without a template, to admins.
     * If verboseOnly is true, only verbose (command: /v) admins see the message.
     * excluded may be zero or one users. If a user is given, the message is hidden from that user
     * and from users that ignore it.
     */
    public void tellAdmin(String message, boolean verboseOnly, MultiUser... excluded) {
        tellAdmin(message, verboseOnly, excluded.length > 0 
                ? u -> !ignores(u, excluded[0]) && (u != excluded[0] || u == consoleDummy)
                : null);
    }

    /**
     * Sends the message, without a template, to admins.
     * If verboseOnly is true, only verbose (command: /v) admins see the message.
     */
    public void tellAdmin(String message, boolean verboseOnly, Predicate<MultiUser> filter) {
        synchronized (users) {
            for (MultiUser u : allUsers) {
                if ((filter == null || filter.test(u)) &&
                        u.isReady() && isAdmin(u) && (u.isVerbose() || !verboseOnly)) {
                    u.schedSend(message);
                }
            }
        }
    }
    
    public void tellAdmin(String message, MultiUser... excluded) {
        tellAdmin(message, false, excluded);
    }
    
    /** Runs the action on all admins */
    public void tellAdmin(Consumer<MultiUser> message) {
        tellRoom(message, this::isAdmin);
    }
    
    /**
     * Sends messages to all admins.
     * tellAdmin(u -> foo(u)) is equivalent to tellAdmin(u -> u.schedSend(foo(u)))
     */
    public void tellAdmin(Function<MultiUser, String> message) {
        tellRoom(u -> u.schedSend(message.apply(u)), this::isAdmin);
    }

    /** Returns whether the user is an admin */
    public boolean isAdmin(MultiUser user) {
        return user == consoleDummy || adminToken == user.getAdminToken();
    }
    
    /** Returns whether the user is an admin and has enabled verbose messages */
    public boolean isVerboseAdmin(MultiUser user) {
        return user.isVerbose() && isAdmin(user);
    }

    /**
     * Changes the admin status of the user named targetName, reporting any error to source.
     * Commands: /!deify, /!undeify
     */
    public void deify(MultiUser source, String targetName, boolean on) {
        MultiUser target = userFromName(targetName);
        if (target == null) {
            source.schedTell("No such user");
        } else if (isAdmin(target) == on) {
            source.schedTell("No change made");
        } else {
            target.setAdminToken(on ? adminToken : null);
            tellAdmin(u -> getDisplayTrivia(u, source) + " has " + (on ? "added " : "removed ") 
                    + getDisplayTrivia(u, target) + " as an admin.");
            tellRoom(u -> behave(u).updateFlagsPrivileged(u, target, null));
            if (!on) {
                target.schedTell(getDisplayNick(target, source) + " has removed you as an admin.");
            }
        }
    }
    
    /** Casts a vote by saviour to kick nuisance from the room. Command: /kick */
    public void voteKick(MultiUser nuisance, MultiUser saviour) {
        Set<MultiUser> voteSet = addMapping(kickVotes, nuisance, saviour);
        try {
            int eligible = 0;
            int votes = 0;
            synchronized (users) {
                for (MultiUser u : users) {
                    if (!u.isReady() || u.isMuted())
                        continue;
                    if (u.idleFor() < lurkingHuman && matureHuman >= 0 && u.age() > matureHuman) {
                        if (u != nuisance)
                            eligible++;
                        if (voteSet.contains(u))
                            votes++;
                    }
                }
            }
            if (eligible == 0)
                eligible = 1;
            if (votes * 1.0 / eligible > 0.65) {
                kickVoted(nuisance);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /** Removes any vote by saviour to kick nuisance. Command: /nokick */
    public void unvoteKick(MultiUser nuisance, MultiUser saviour) {
        removeMapping(kickVotes, nuisance, saviour);
    }
    
    /** Suspends pulse connections for the given time. Command: /!pausepulse */
    public void pausePulse(long duration) {
        disablePulseTime = System.currentTimeMillis();
        if (pulseFreq > 0)
            lastPulse = disablePulseTime - pulseFreq;
        pulseCooldownTime = Math.max(0, duration);
        if (pulseCooldownTime > 0)
            tellAdmin("The pulse has been paused for " + Util.minSec(pulseCooldownTime));
    }
    
    /** Returns the time until the pulse resumes, or 0 if it is not paused. */
    public long getPulseCooldown() {
        return Math.max(0, disablePulseTime - System.currentTimeMillis() + pulseCooldownTime);
    }
    
    protected void notifyPulseResumed() {
        if (getPulseCooldown() == 0 && pulseCooldownTime != 0) {
            if (shouldAddPulse())
                tellAdmin("The room has resumed pulsing.");
            else
                tellAdmin("The pulse cooldown has expired, but another setting is preventing pulses from being sent.");
            pulseCooldownTime = 0;
        }
    }
    
    /** Adds a connection for returning users (pulse). Command: /!invitep */
    public boolean addPulse() {
        synchronized (users) {
            for (MultiUser other : users)
                if (other.isPulse()) {
                    tellAdmin("Refusing to add more than one pulse! Try kicking the existing one first.");
                    return false;
                }
        }
        proxies.switchIfUnusable();
        boolean accepted;
        synchronized (toothpasteTypes) {
            accepted = !toothpasteTypes.contains(JoinType.PULSE);
        }
        MultiUser uc = makeIdUser().fill(accepted, false, proxies.current());
        uc.pulse();
        synchronized (users) {
            users.add(uc);
            uc.start();
        }
        return true;
    }

    /**
     * Returns the probabilities set for each join type in a map suitable for Util.randomize().
     * If types is specified, only those types are given a probabilty above 0.
     */
    public EnumMap<JoinType, Double> getJoinWeights(EnumSet<JoinType> types) {
        if (types == null)
            types = EnumSet.allOf(JoinType.class);
        double nonText = qfreq + bareFreq;
        EnumMap<JoinType, Double> weights = new EnumMap<>(JoinType.class);
        weights.put(JoinType.TEXT, Math.max(0, 1 - nonText));
        weights.put(JoinType.QUESTION, qfreq / Math.max(1, nonText));
        weights.put(JoinType.BARE, bareFreq / Math.max(1, nonText));
        for (Map.Entry<JoinType, Double> entry : weights.entrySet())
            if (!types.contains(entry.getKey()))
                entry.setValue(0d);
        return weights;
    }
    
    /**
     * Returns the given topics, minus any topics that current connections are listening on.
     * Used to prevent the room from connecting to itself.
     */
    public String[] getAvailableTopics(String[] topics) {
        synchronized (users) {
            for (MultiUser u : users)
                topics = Util.filter(topics, topic -> !u.isListeningOnTopic(topic));
        }
        return topics;
    }

    @Deprecated
    public boolean add(Boolean forceQ, String... params) {
        if (forceQ == null)
            return add(EnumSet.of(JoinType.TEXT));
        else if (forceQ)
            return add(EnumSet.of(JoinType.QUESTION));
        else
            return add(EnumSet.allOf(JoinType.class));
    }

    /**
     * Adds a user of one of the given types, passing the given parameters to Omegle if appropriate.
     * Possible parameters are lang (the code of a language to prefer) and topics (comma-separated
     * interests to listen on, overriding the default topics).
     * Returns whether the user will immediately enter the room without having to type the entry word.
     * Commands: /invite, /!invite, /!inviteq, /!invitec, /!inviteb, /!invitet
     */
    public boolean add(EnumSet<JoinType> types, String... params) {
        JoinType type = Util.randomize(getJoinWeights(types));
        
        String lang = null;
        String[] topics = null;
        
        if (params != null) {
            for (int i = 1; i < params.length; i += 2)
                if ("lang".equals(params[i - 1]))
                    lang = params[i];
                else if ("topics".equals(params[i - 1]))
                    topics = params[i].split(",");
        }
        proxies.switchIfUnusable();
        boolean accepted;
        boolean useDefaultTopics = topics == null;
        if (useDefaultTopics)
            topics = defaultTopics;

        synchronized (users) {
            if (type == JoinType.TEXT) {
                topics = getAvailableTopics(topics);
                if (useDefaultTopics)
                    topics = Util.randomize((int) Math.round(topicSample * topics.length), topics);
                if (topics.length == 0)
                    type = JoinType.BARE;
            }
            synchronized (toothpasteTypes) {
                accepted = !toothpasteTypes.contains(type);
            }
            MultiUser uc = makeIdUser().fill(accepted, type == JoinType.QUESTION, proxies.current());
            if (lang != null)
                uc.withLang(lang);
            if (type == JoinType.TEXT)
                uc.withTopics(topics);
            users.add(uc);
            uc.start();
        }
        return accepted;
    }
    
    /** Ensures that the array of numeric user IDs is comfortably large */
    protected void resizeIds() {
        int desired = Math.max(users.size() + 1, maxTotal()) * 2 + 1;
        synchronized (users) {
            if (users.size() >= ids.length - 2) {
                ids = Arrays.copyOf(ids, desired);
            } else if (ids.length > Math.max(users.size(), maxTotal()) * 3) {
                for (int i = desired; i < ids.length; i++)
                    if (ids[i] != null || i == lastFreedId)
                        desired = i + 1;
                ids = Arrays.copyOf(ids, desired);
            }
        }
    }
    
    /** Returns the lowest free numeric user ID */
    protected int freeId() {
        synchronized (users) {
            for (int i = 0; i < ids.length; i++)
                if (ids[i] == null && i != lastFreedId)
                    return i;
        }
        throw new RuntimeException("No free IDs, can't add user!");
    }

    /** Adds a user using the default weights and (if applicable) topics */
    public boolean add() {
        return add(false);
    }
   
    private static Pattern nonword = Pattern.compile("\\W+");
    private static String canonicalTopic(String topic) {
        return nonword.matcher(topic).replaceAll("");
    }
    
    /** Sets the default interests used to find new users. Command: /!topics */
    public void setDefaultTopics(String... topics) {
        if (topics == null) return;
        defaultTopics = Util.map(topics, Multi::canonicalTopic, String[]::new);
    }
    
    /** Adds to the default interests used to find new users. Command: /!addtopics */
    public void addTopics(String... topics) {
        topics = Util.map(topics, Multi::canonicalTopic, String[]::new);
        topics = Util.filter(topics, t -> !t.isEmpty() && !Util.contains(defaultTopics, t));
        defaultTopics = Util.concat(defaultTopics, topics);
    }
    
    /** Removes from the default interests used to find new users. Command: /!rmtopics */
    public void removeTopics(String... topics) {
        defaultTopics = Util.filter(defaultTopics, t -> !Util.contains(topics, canonicalTopic(t)));
    }
    
    /** Returns the default topics */
    public String[] getDefaultTopics() {
        return defaultTopics.clone();
    }
    
    /** Returns the default topics as a user-facing string */
    public String getTopicList() {
        if (defaultTopics.length == 0)
            return "The default topic list is empty.";
            
        return "Default topics: " + String.join(", ", defaultTopics);
    }

    /**
     * Removes a user from the room, notifies users, and does related cleanup.
     * This stops the session on our side, but doesn't notify Omegle. See kick() for that.
     */
    protected void remove(MultiUser user) {
        if (user == null) {
            System.err.println("warn: removing null user!");
            return;
        }
        if (user == consoleDummy) {
            tellRoom(u -> behave(u).tellLeft(u, user));
            return;
        }
        boolean wasIn;
        synchronized (users) {
            int number = user.getNumber();
            wasIn = ids.length > number && ids[number] == user;
        }
        user.dispose();
        synchronized (users) {
            lastFreedId = user.getNumber();
            ids[lastFreedId] = null;
            users.remove(user);
        }
        if (wasIn && user.didLive()) {
            tellRoom(u -> behave(u).tellLeft(u, user));
        }
        if (users.isEmpty() && shouldRevive()) {
            Util.sleep(10000);
            autoJoin = true;
        }
        removeCommon(user);
    }
    
    protected void removeCommon(MultiUser user) {
        user.dispose();
        clearMappings(kickVotes, user);
        clearMappings(ignored, user);
        synchronized (historyLock) {
            for (Message message : history)
                if (message.user == user)
                    message.user = null;
        }
    }
    
    /** Returns whether any user in the room is typing */
    public boolean isTyping() {
        int count = 0;
        synchronized (users) {
            for (MultiUser u : users)
                if (u.isTyping() && u.isAccepted() && !u.isMuted())
                    count++;
        }
        return count != 0;
    }

    /** Handles the Omegle typing event, repeating it to the remaining users if needed. */
    protected void typing(MultiUser user) {
        if (user.isAccepted() && !user.isMuted() && !isTyping()) {
            synchronized (users) {
                for (MultiUser u : users) {
                    if (u != user && u.isReady()) {
                        u.schedSendTyping(true);
                    }
                }
            }
        }
    }
    
    /** Handles the Omegle stoppedTyping event, repeating it to the remaining users if needed. */
    protected void stoppedTyping(MultiUser user) {
        if (user.isAccepted() && !user.isMuted() && isTyping()) {
            synchronized (users) {
                for (MultiUser u : users) {
                    if (u != user && u.isReady()) {
                        u.schedSendTyping(false);
                    }
                }
            }
        }
    }
    
    /** Handles messages from users that need to say the entry word to enter the room. */
    public void hearUntriaged(MultiUser user, String data) {
        if (isEntryLine(data)) {
            user.accept();
            welcome(user);
        } else if (user.isQuestionMode()) {
            return;
        } else if (user.getMsgCount() > 2) {
            kick(user, t("kickspam"));
        } else {
            user.schedTell(t("saytoothpaste", toothpastePrompt));
        }
    }
    
    /** Returns whether the given message contains the entry word */
    protected boolean isEntryLine(String line) {
        return !(line.startsWith("|") && line.contains("\""))
            && toothpastePattern.matcher(line).find();
    }
    
    /** Getter and setter for the regex to match the entry word. Command: /!toothpastepattern */
    public String getEntryPattern() { return toothpastePattern.toString(); }
    public void setEntryPattern(String entryPattern) { toothpastePattern = Pattern.compile(entryPattern); }
    
    /** Getter and setter for the entry word as shown to users. Commands: /!toothpasteprompt */
    public String getEntryPrompt() { return toothpastePrompt; }
    public void setEntryPrompt(String entryPrompt) { toothpastePrompt = entryPrompt; }
    
    /** Returns the ban expressions that match the given message. Command: /!checkban */
    protected Collection<Ban> checkBans(String data) {
        List<Ban> matched = new ArrayList<>();
        for (Ban ban : bans)
            if (ban.check(this, null, data) != Ban.Action.PASS)
                matched.add(ban);
        return matched;
    }
    
    /** Performs any ban action that applies to the user saying the message, and returns which action was performed */
    protected Ban.Action runBans(MultiUser user, String data) {
        Ban.Action action = Ban.Action.PASS;
        StringBuilder matches = new StringBuilder();
        String comment = null;
        for (Ban ban : bans) {
            Ban.Action current = ban.check(this, user, data);
            if (current != Ban.Action.PASS) {
                matches.append("\nto a " + current + " for pattern " + Objects.toString(ban.comment, ban.pattern));
                if (current.supersedes(action)) {
                    action = current;
                    if (ban.comment != null)
                        comment = ban.comment;
                }
                if (action.isHighest())
                    break;
            }
        }
        if (matches.length() > 0 && (!user.isMuted() || action != Ban.Action.MUTE && action != Ban.Action.MUTE_ALL))
            tellAdmin(user + " sentenced " + matches.substring(1), false, to -> !ignores(to, user));
        if (action != Ban.Action.PASS) {
            relay("special", user, data, action.toString(), user.isMuted() ? this::isVerboseAdmin : this::isAdmin);
            switch (action) {
            case SOFT:
                user.schedTell(comment == null ? t("softban") : t("softbancomment", comment));
                break;
            case WARN:
                user.schedTell(comment == null ? t("warnban") : t("warnbancomment", comment));
                user.warn();
                break;
            case KICK:
                kickBot(user);
                break;
            case MUTE:
            case MUTE_ALL:
                mute(user);
                break;
            default:
                throw new IllegalStateException("weird ban action");
            }
        }
        return action;
    }
    
    /** 
     * Handles messages from users (the Omegle message event).
     * If the message looks like a message from another room, kicks the user.
     * If it matches a ban expression, takes the relevant action.
     * If the user is not yet in the room, checks for the entry word.
     * Checks for flood limits and suppresses message if needed.
     * Runs commands, or if message is bare, sends it to the users that should see it.
     */
    public void hear(MultiUser user, String data) {
        stoppedTyping(user);
        if (user.getMsgCount() < 3) {
            if (Util.isTailEat(data)) {
                relay("special", user, data, "TAIL", this::isVerboseAdmin);
                kick(user, t("taileat"));
                return;
            }
        }
        Ban.Action banAction = data.matches("/[!.].*") && isAdmin(user) ? Ban.Action.PASS : runBans(user, data);
        if (banAction != Ban.Action.PASS && banAction != Ban.Action.MUTE) {
            suppressFlood(user);
            return;
        }
        if (!user.isAccepted()) {
            if (banAction == Ban.Action.PASS)
                relay("special", user, data, "unco", this::isVerboseAdmin);
            isDuplicate(user, data);
            hearUntriaged(user, data);
            return;
        }
        boolean showVerbose = banAction == Ban.Action.PASS && !data.toLowerCase().startsWith("/password");
        if (isSilentCommand(user, data)) {
            if (showVerbose)
                relay("special", user, data, "cmd", this::isVerboseAdmin);
            command(user, data);
        } else if (suppressFlood(user)) {
            if (showVerbose)
                relay("special", user, data, "flood", this::isVerboseAdmin);
            user.schedTell(t("floodtoofast", Double.toString(floodControlTime / 1000.0)));
        } else if (isDuplicate(user, data)) {
            if (showVerbose)
                relay("special", user, data, "dup", this::isVerboseAdmin);
        } else if (data.startsWith("/")) {
            if (showVerbose)
                relay("special", user, data, "cmd", this::isVerboseAdmin);
            command(user, data);
        } else {
            relay("normal", user, data);
            if (user.isMuted()) {
                if (showVerbose)
                    relay("special", user, data, "mute", to -> isVerboseAdmin(to) && !to.isMuted());
            }
        }
    }
    
    /** Returns whether the first user ignores the second user */
    public boolean ignores(MultiUser to, MultiUser from) {
        Set<MultiUser> ignoredUsers = ignored.get(to);
        return ignoredUsers != null && ignoredUsers.contains(from);
    }
    
    /** Returns the users that the given user ignores */
    public Collection<MultiUser> getIgnored(MultiUser to) {
        Collection<MultiUser> ignoredUsers = ignored.get(to);
        if (ignoredUsers == null || ignoredUsers.size() == 0)
            return null;
        List<MultiUser> ret;
        synchronized (ignored) {
            ret = new ArrayList<>(ignoredUsers);
        }
        Collections.sort(ret, Comparator.comparingLong(a -> -a.age()));
        return ret;
    }
    
    /**
     * Sends the message from the given user to muted users.
     * If me is true, the message is sent using the action template.
     */
    protected void sendToMuted(MultiUser from, String data, boolean me) {
        if (!me && data.startsWith("/me ")) {
            data = data.substring(data.indexOf(" ") + 1);
            me = true;
        }
        synchronized (users) {
            for (MultiUser u : users) {
                if ((me || u != from) && u.isAccepted() && u.isMuted() && !ignores(u, from)) {
                    u.schedSend(t(me ? "action" : "normal", getDisplayNick(u, from), data));
                }
            }
        }
    }
    
    /**
     * Returns whether the given message, when sent by the given user, would be invisible to
     * other (non-admin) users.
     */
    public boolean isSilentCommand(MultiUser user, String data) {
        return data.startsWith("/") && behave(user).isSilent(data.substring(1).split(" ")[0]);
    }
    
    /**
     * Handles messages that are commands.
     * The commands /challenge, /password, /v, and /t are handled directly.
     * Other commands are handled by adminCommands, userBehaviour or botBehaviour.
     */
    public void command(MultiUser user, String data) {
        String[] ca = data.split(" ", 2);
        if (ca[0].equalsIgnoreCase("/challenge")) {
            user.schedTell("Login challenge: " + user.getID() + " " + Util.sha1(user.getID() + challenge));
        } else if (ca.length > 1 && ca[0].equalsIgnoreCase("/password")) {
            if (ca[1].equals(password) || ca[1].equals(Util.sha1(user.getID() + password))) {
                user.setAdminToken(adminToken);
                tellAdmin(to -> getDisplayNick(to, user) + " has logged in as admin");
            } else {
                user.setAdminToken(null);
                System.out.println("Failed login with " + ca[1]);
                tellAdmin(to -> getDisplayNick(to, user) + " has entered a wrong password");
                user.schedTell(t("wrongpassword"));
            }
            tellRoom(u -> behave(u).updateFlagsPrivileged(u, user, null));
        } else if (ca[0].matches("/[!.].*")) {
            if (isAdmin(user)) {
                commands.adminCommand(ca.length == 1 ? ca[0].substring(2) : ca[0].substring(2) + " " + ca[1], user);
            } else {
                user.schedTell(t("wrongpassword"));
            }
        } else if (ca[0].equalsIgnoreCase("/v")) {
            user.setVerbose(true);
        } else if (ca[0].equalsIgnoreCase("/t")) {
            user.setVerbose(false);
        } else if (!data.isEmpty()) {
            behave(user).command(data.substring(1), user);
        }
    }
    
    /** Returns which UserBehaviour instance should be used for the given user */
    protected UserBehaviour behave(MultiUser user) {
        return user.isBot() ? botBehaviour : userBehaviour;
    }
    
    /** Adds a new user. Implementation of command: /invite */
    protected void invite(MultiUser user, String... args) {
        if (user.isMuted()) {
            user.schedTell(t(toothpasteTypes.contains(JoinType.QUESTION) ? "invitedc" : "invitedq", 
                    user.getDisplayNick()));
        } else if (System.currentTimeMillis() - lastInvite < minInterval()) {
            user.schedTell(t("invitetoofast"));
        } else if (users.size() >= maxTotal() || spamSize() >= maxChatting()) {
            user.schedTell(t("invitefull"));
        } else {
            lastInvite = System.currentTimeMillis();
            if (add(false, args)) {
                relay("system", user, t("invitedq", user.getDisplayNick()));
            } else {
                relay("system", user, t("invitedc", user.getDisplayNick()));
            }
        }
    }
    
    /** Sends a private message. Implementation of command: /pm */
    protected void pm(MultiUser user, String... p) {
        user.recordMessage();
        MultiUser dest = userFromName(p[0]);
        if (dest == null) {
            user.schedTell(t("pmnouser"));
        } else {
            if (user.isLurker() && !dest.isLurker() && dest != consoleDummy) {
                user.schedTell(t("pmlurker"));
                return;
            }
            if ((!user.isMuted() || dest == consoleDummy || dest.isMuted()) && !ignores(dest, user)) {
                dest.schedSend(t("special", getDisplayNick(dest, user), p[1], t("private")));
                user.schedTell(t("pmsent"));
            } else {
                user.schedTell(t("pmsent"));
            }
        }
    }
    
    /** Sends an action message. Implementation of command: /me */
    protected void me(MultiUser user, String message) {
        relay("action", user, message);
    }
    
    /** Changes the user's name. Implementation of command: /nick */
    protected void nick(MultiUser user, String nick) {
        nick(user, nick, false);
    }
    
    /** Changes the user's name, optionally silently. Implementation of command: /!silentnick */
    protected void nick(MultiUser user, String nick, boolean silent) {
        nick = nick.replace(" ", "-").replace("\n", "").replace("\r", "");
        if (nick.matches("[0-9]+")) {
            user.schedTell(t("nickdigits"));
        } else {
            try {
                String oldNick = user.getNick();
                if (nick.length() > 100)
                    nick = nick.substring(0, 100);
                user.setNick(nick);
                tellRoom(u -> behave(u).tellNickChanged(u, user, oldNick, silent));
            } catch (IllegalArgumentException e) {
                user.schedTell(t("nicktaken"));
            }
        }
    }
    
    /** Adds to the user's ignore list. Implementation of command: /ignore */
    protected void ignore(MultiUser user, String name) {
        MultiUser dest = userFromName(name);
        if (dest == null) {
            user.schedTell(t("pmnouser"));
        } else if (dest == user || dest == consoleDummy) {
            user.schedTell(t("cannotignore"));
        } else if (ignores(user, dest)) {
            user.schedTell(t("alreadyignored", dest.getDisplayNick()));
        } else {
            addMapping(ignored, user, dest);
            behave(user).updateFlags(user, dest, t("addignore", dest.getDisplayNick()));
        }
    }
    
    /** Removes from the user's ignore list. Implementation of command: /unignore */
    protected void unignore(MultiUser user, String name) {
        MultiUser dest = userFromName(name);
        if (dest == null) {
            user.schedTell(t("pmnouser"));
        } else if (!ignores(user, dest)) {
            user.schedTell(t("notignored", dest.getDisplayNick()));
        } else {
            removeMapping(ignored, user, dest);
            behave(user).updateFlags(user, dest, t("removeignore", dest.getDisplayNick()));
        }
    }
    
    /** Clears the user's ignore list. Implementation of command: /clearignore */
    protected void clearIgnore(MultiUser user) {
        synchronized (ignored) {
            ignored.remove(user);
        }
        user.schedTell(t("ignorecleared"));
    }

    /** Kind of like original Facebook's pokes but less ceremonious. Implementation of command: /pat */
    protected void patUser(MultiUser source, String name) {
        MultiUser target = userFromName(name);
        if (target == null) {
            source.schedTell(t("pmnouser"));
        } else if (source == target) {
            source.schedTell(t("noselfpat"));
        } else if (System.currentTimeMillis() - source.lastPat < patCooldown) {
            source.schedTell(t("pattoofast"));
        } else {
            target.patCount++;
            source.lastPat = System.currentTimeMillis();
            relay("system", source, t("patuser", source.getDisplayNick(), target.getDisplayNick(), target.patCount));
        }
    }
    
    /** Records a vote to kick. Implementation of command: /kick */
    protected void slashkick(MultiUser user, String name) {
        if (userFromName(name) == null) {
            user.schedTell(t("kicknouser"));
        } else if (matureHuman < 0) {
            user.schedTell(t("commanddisabled"));
        } else if (user.age() < matureHuman) {
            user.schedTell(t("kicktooyoung"));
        } else if (user.isMuted()) {
            tellMuted(t("kickvote", user.getDisplayNick(), userFromName(name).getDisplayNick()));
        } else {
            MultiUser toKick = userFromName(name);
            relay("system", user, t("kickvote", user.getDisplayNick(), toKick.getDisplayNick()));
            voteKick(toKick, user);
        }
    }
    
    /** Removes a vote to kick. Implementation of command: /nokick */
    protected void slashdontkick(MultiUser user, String name) {
        if (userFromName(name) == null) {
            user.schedTell(t("dontkicknouser"));
        } else if (user.isMuted()) {
            tellMuted(t("dontkickvote", user.getDisplayNick(), userFromName(name).getDisplayNick()));
        } else {
            MultiUser toKick = userFromName(name);
            unvoteKick(toKick, user);
            relay("system", user, t("dontkickvote", user.getDisplayNick(), toKick.getDisplayNick()));
        }
    }
    
    /** Enables adding of new users (spamming). Implementation of command: /spam */
    protected void spam(MultiUser user) {
        spam(user, false);
    }
    
    /** Enables adding of new users (spamming), optionally silently. Implementation of command: /!spam */
    protected void spam(MultiUser user, boolean silent) {
        long joinRestSec = joinFreq() * 3 / 2000;
        long joinFreqSec = joinFreq() / 1000;
        if (autoJoin) {
            if (proxies.isBanned()) {
                user.schedTell(t("spambanned"));
            } else if (spamSize() >= maxChatting()) {
                user.schedTell(t("spampausedfull", maxChatting()));
            } else if (users.size() >= maxTotal()) {
                user.schedTell(t("spampausedlingering", joinRestSec));
            } else {
                user.schedTell(t("spamalreadyon", joinFreqSec));
            }
        } else if (user.isMuted() && !silent) {
            relay("system", user, t("spamon", user.getDisplayNick()));
        } else {
            Consumer<String> tell = silent ? msg -> tellAdmin(msg) : msg -> relay("system", user, msg);
            autoJoin = true;
            tell.accept(t("spamon", user.getDisplayNick()));

            if (proxies.isBanned()) {
                tell.accept(t("spambanned"));
            } else if (spamSize() >= maxChatting()) {
                tell.accept(t("spampausedfull", maxChatting()));
            } else if (users.size() >= maxTotal()) {
                tell.accept(t("spampausedlingering", joinRestSec));
            }
        }
    }
    
    /** Disables adding of new users (spamming). Implementation of command: /nospam */
    protected void nospam(MultiUser user) {
        nospam(user, false);
    }
    
    /** Disables adding of new users (spamming), optionally silently. Implementation of command: /!nospam */
    protected void nospam(MultiUser user, boolean silent) {
        if (!autoJoin) {
            user.schedTell(t("spamalreadyoff"));
        } else {
            if (silent || !user.isMuted())
                autoJoin = false;
            if (silent)
                tellAdmin(t("spamoff", user.getDisplayNick()));
            else
                relay("system", user, t("spamoff", user.getDisplayNick()));
        }
    }
    
    /** Sends a message to the admins. Implementation of command: /heyadmin */
    protected void heyadmin(MultiUser user, String message) {
        String adminMessage = user + " is calling you";
        if (!message.isEmpty())
            adminMessage += ": " + message;
        tellAdmin(adminMessage, false, user);
        relay("system", user, t("admincalled", user.getDisplayNick()));
        if (!user.isMuted() && !ignores(consoleDummy, user))
            System.out.println("\n\n\nSomeone fucking called you!\n\n\n");
    }

    /** Sends the user some information about itself. Implementation of command: /id */
    protected void idSelf(MultiUser user) {
        String pulse;
        synchronized (users) {
            pulse = users.stream()
                .filter(MultiUser::isPulse)
                .map(u -> t("pulseid", u.getNick(), u.getPulseWords(), u.getRandid(), Util.minSec(u.idleFor())))
                .findFirst().orElse(t("idnopulse"));
        }
        user.schedTell(t("idreply", user.getNickAndTrivia(), pulse, chatName, MultiUser.MOTHERSHIP));
    }
    
    /** Sends the user some information about another user. Implementation of command: /id (with a parameter) */
    protected void idOther(MultiUser user, String name) {
        MultiUser dest = userFromName(name);
        if (dest == null)
            user.schedTell(t("pmnouser"));
        else
            user.schedTell(dest.getNickAndTrivia());
    }
    
    /** Notifies other users of a user joining, and returns a list of other users to be shown to the new user. */
    protected StringBuilder welcomeNotifyAndCollect(MultiUser user) {
        StringBuilder otherUsers = new StringBuilder();
        tellRoom(u -> {
            behave(u).tellJoined(u, user);
            if (u != consoleDummy && !u.isLurker() && (countMuted || user.isMuted()))
                synchronized (otherUsers) {
                    otherUsers.append(", ").append(u.getNick());
                }
        }, u -> u != user && u.isReady());
        return otherUsers;
    }
    
    /**
     * Welcomes a user and notifies others of this user joining.
     * If isInformed is set, the welcome message is shortened slightly.
     */
    protected void welcomeTriaged(MultiUser user, boolean isInformed) {
        boolean separateFirstLine = Math.random() > 0.66;
        String firstLine = randomize(tRandom("youHaveEnteredSingle"), tRandom("youHaveEnteredSingle"), tRandomLine("youHaveEntered")) 
            + tRandomLine("rovingGroupChat");
        if (!isInformed && separateFirstLine) {
            user.schedTell(firstLine);
        }

        if (spamSize() < 1) {
            user.schedTell(tRandomLine("firstUserLine")
                    .replace("__room__", tRandom("room"))
                    + t("emptyRoomUsername", user.getNick()));
            if (shouldRevive()) {
                add();
            }
        } else {
            StringBuilder otherUsers = welcomeNotifyAndCollect(user);
            boolean showUsers = otherUsers.length() > 2 && Math.random() > 2d / 5;
            int userCount = showUsers ? listSize(user.isMuted(), false) : spamSize();
            userCount--;
            String countLine = tRandom("nOthers").replace("__room__", tRandom("room")).replace("__others__",
                    tRandom("others")).replace("__num__", String.valueOf(userCount)) + (showUsers ? ": " : ".");
            if (!separateFirstLine && !isInformed && Math.random() > 0.5) {
                user.schedTell(firstLine + countLine);
            } else {
                user.schedTell(countLine);
            }
            if (showUsers) {
                if (listSize(user.isMuted(), false) > 5 || Math.random() > 0.5) {
                    user.schedTell("    " + otherUsers.substring(2));
                } else {
                    for (String otherUser : otherUsers.substring(2).split(", ")) {
                        user.schedTell("    " + otherUser);
                    }
                }
            }
            user.schedTell(t("normalUsername", user.getNick()));
        }
        user.schedTell(t("shortHelp"));
    }
    
    /** Informs the user that they have encountered a room and must say the entry word to enter */
    protected void inform(MultiUser user) {
        Util.sleep((int) (Math.random() * 700));

        String stumbledLine = tRandomLine("stumbledLine");
        String flauntLine = tRandomLine("flauntLine");
        String pasteLine = String.format(tRandom("sayToothpasteToEnter").replace("__room__", tRandom("room")), toothpastePrompt);

        if (Math.random() < 0.5) {
            if (Math.random() < 0.5) {
                user.schedTell(stumbledLine);
                user.schedTell(flauntLine);
                user.schedTell(pasteLine);
            } else {
                user.schedTell(stumbledLine + flauntLine);
                user.schedTell(pasteLine);
            }
        } else {
            if (Math.random() < 0.5) {
                user.schedTell(stumbledLine);
                user.schedTell(flauntLine + pasteLine);
            } else {
                user.schedTell(stumbledLine);
                user.schedTell(pasteLine + flauntLine);
            }
        }

        if (spamSize() < 2) {
            user.schedTell("(" + tRandomLine("emptyLine").replace("__room__", tRandom("room")) + ")");
        }
        user.inform();
    }

    /** Handles a user joining. Calls welcomeTriaged() or inform() depending on whether they must type the entry word. */
    protected void welcome(MultiUser user) {
        if (user.isAccepted()) {
            welcomeTriaged(user, user.isInformed());
        } else if (!user.isInformed()) {
            inform(user);
        }

    }
    
    /** Sends a system message to a specific user. Implementation of command: /!annpm */
    protected void annPrivate(MultiUser from, String to, String message) {
        MultiUser toUser = userFromName(to);
        if (toUser == null) {
            from.schedTell("No such user");
        } else {
            toUser.schedTell(message);
        }
    }
    
    /** Clears all users' ignore lists. Implementation of command: /!clearignorelists */
    protected void clearAllIgnore() {
        synchronized (ignored) {
            ignored.clear();
        }
        tellAdmin("All ignore lists have been cleared!");
    }

    protected void kickVoted(MultiUser u) {
        kick(u, t("kickvoted"));
    }

    protected void kickInactive(MultiUser u) {
        kick(u, t("kickinactive"));
    }

    protected void kickBot(MultiUser u) {
        kick(u, t("kickspam"));
    }

    protected void kickFlood(MultiUser u) {
        kick(u, t("kickflood", floodKickSize, Double.toString(floodKickTime / 1000.0)));
    }

    /** Kicks the user, showing the given message as a reason to the remaining users */
    protected void kick(MultiUser u, String message) {
        if (u == null) {
            System.err.println("not a user (tried to kick: " + message + ")");
        } else {
            u.setKickReason(message);
            u.schedSendDisconnect();
            remove(u);
            if (!u.didLive())
                System.out.println(u.getNick() + " has been kicked: " + message);
        }
    }

    /** Kicks the user with the given message. Implementation of commands: /!kick, /!customkick */
    boolean kick(String name, String message) {
        MultiUser u = userFromName(name);
        if (u == null)
            return false;
        kick(u, message);
        return true;
    }

    /** Mutes the user. Muted users can only be seen by other muted users and verbose admins. */
    protected void mute(MultiUser u) {
        if (!u.isMuted()) {
            u.setMuted(true);
            tellRoom(target -> behave(target).updateFlagsPrivileged(target, u, 
                    "Muted: " + getDisplayNick(target, u)));
        }
    }

    /** Mutes the user. Implementation of command: /!mute */
    boolean mute(String name) {
        MultiUser u = userFromName(name);
        if (u == null)
            return false;
        mute(u);
        return true;
    }

    /** Unmutes the user. */
    protected void unmute(MultiUser u) {
        if (u.isMuted()) {
            u.setMuted(false);
            tellRoom(target -> behave(target).updateFlagsPrivileged(target, u, 
                    "Unmuted: " + getDisplayNick(target, u)));
        }
    }

    /** Unmutes the user. Implementation of command: /!unmute */
    boolean unmute(String name) {
        MultiUser u = userFromName(name);
        if (u == null)
            return false;
        unmute(u);
        return true;
    }
    
    /** Fakes a message notifying that the user has joined. Implementation of command: /!showjoin */
    boolean showJoin(String name) {
        MultiUser u = userFromName(name);
        if (u == null)
            return false;
        tellRoom(target -> userBehaviour.tellJoined(target, u));
        return true;
    }
    
    /** Fakes a message notifying that the user has left. Implementation of command: /!showleave */
    boolean showLeave(String name, String reason) {
        MultiUser u = userFromName(name);
        if (u == null)
            return false;
        tellRoom(target -> userBehaviour.tellLeft(target, u, reason, true));
        return true;
    }
    
    /** Appends the user's admin-facing flags to the builder. See /!flaghelp for the flags' meanings. */
    public StringBuilder appendFlags(MultiUser u, StringBuilder list) {
        if (list == null)
            list = new StringBuilder();
        if (u.isPulse())
            list.append('P');
        else if (!u.isAccepted())
            list.append('U');
        if (!u.isCreated())
            list.append('Z');
        else if (u.getCaptchaSiteKey() != null)
            list.append("C");
        else if (!u.isConnected())
            list.append('W');
        if (u.isMuted())
            list.append('M');
        if (isAdmin(u))
            list.append('A');
        return list;
    }
    
    /** Appends the topic's flags, as seen by target, to the builder. */
    public StringBuilder appendFlags(MultiUser target, MultiUser topic, StringBuilder list) {
        // why is isPrivileged in UserBehaviour again?
        boolean privileged = behave(target).isPrivileged(target);
        if (privileged)
            list = appendFlags(topic, list);
        else if (list == null)
            list = new StringBuilder();
        
        if (topic.getLurkPreference() == Boolean.FALSE)
            list.append('T');
        else if (topic.isLurker())
            list.append('8');
        if (privileged && ignores(topic, target))
            list.append("D");
        if (ignores(target, topic))
            list.append("I");
        return list;
    }
    
    /**
     * Adds the current time in seconds to the given map under the key "time".
     * Used when sending JSON messages to users declared as bots.
     * The bot argument is ignored.
     */
    public Map<String, Object> fillBotMessage(MultiUser bot, Map<String, Object> message) {
        message.put("time", System.currentTimeMillis() / 1000);
        return message;
    }
    
    /** Updates the user's soft flood control meter and returns whether a message from this user should be suppressed. */
    protected boolean suppressFlood(MultiUser user) {
        if (floodControlSpan == 0 || floodControlTime < 1 || floodControlSpan > 0 && user.getMsgCount() >= floodControlSpan)
            return false;
        if (user.floodControlMeter.allow(1, floodControlTime))
            return false;
        if (user.getMsgCount() >= 1)
            user.setMsgCount(user.getMsgCount() - 1);
        return true;
    }
    
    /**
     * Updates the user's hard flood control meter and warns or kicks them if needed.
     * Returns whether the message should be suppressed.
     * The data argument is ignored.
     */
    protected boolean kickIfFlood(MultiUser user, String data) {
        if (user.floodKickMeter.allow(floodKickSize, floodKickTime))
            return false;
        if (matureHuman < 0 || user.age() < matureHuman || user.isWarned()) {
            kickFlood(user);
        } else {
            user.warn();
            relay("system", user, t("floodwarning", floodKickSize, 
                    String.valueOf(floodKickTime / 1000.0), user.getDisplayNick()));
        }
        return true;
    }
    
    /** Callback method for the Proxies instance to notify admins of proxy-related events. */
    public void proxyMessage(String message) {
        tellAdmin(message);
    }

    /** Master callback method called whenever a user connection receives an event from Omegle. */
    @Override
    public void callback(MultiUser user, String method, String data) {
        synchronized (users) {
            if (user != null && !users.contains(user) && !user.isDummy())
                return;
        }
        String sanitizedData = data == null ? ""
            : data.replaceAll("[\u0000-\u0008\u000e-\u001f\u007f-\u009f]", "");
        //System.out.println("callback: '" + user + "', '" + method + "', '" + data + "'");
        if (method.equals("captcha")) {
            if (user.isPulseEver())
                lastPulse = 0;
            captchad(user, data);
        } else if (method.equals("ban")) {
            if (user.isPulseEver())
                lastPulse = 0;
            banned(user.getProxy(), sanitizedData);
        } else if (method.equals("died")) {
            if (user.isPulseEver())
                lastPulse = 0;
            user.getProxy().dirty();
            tellAdmin("Connection died: " + user);
            remove(user);
            if (proxies.switchProxy())
                mainLoop(true);
        } else if (method.equals("disconnected")) {
            remove(user);
        } else if (method.equals("typing")) {
            typing(user);
        } else if (method.equals("stoppedtyping")) {
            stoppedTyping(user);
        } else if (method.equals("message")) {
            if (!kickIfFlood(user, sanitizedData))
                hear(user, sanitizedData);
        } else if (method.equals("connected")) {
            welcome(user);
        } else if (method.equals("interests")) {
            if (user.isAccepted())
                tellAdmin("Joined with " + sanitizedData);
            else
                tellAdmin(to -> getDisplayNick(to, user) + " connected with " + sanitizedData);
        } else if (method.equals("pulse success")) {
            if (user.isAccepted())
                tellAdmin("Pulse, joined with " + sanitizedData);
            else
                tellAdmin(to -> getDisplayNick(to, user) + " pulse, connected with " + sanitizedData);
            addPulse();
        } else if (method.equals("chatname")) {
            chatName = sanitizedData;
        } else {
            System.err.println("\n\n\nUNKNOWN CALLBACK METHOD in Multi: '" + method + "'\n\n\n");
        }
    }

    /** Hook called before setUp() */
    protected void beforeRun() { }
    
    /** Creates the dummy user and loads any rc (command) and json (serialized state) files */
    protected void setUp() {
        consoleDummy = makeIdUser().dummy();
        if (rcFiles.isEmpty())
            commands.loadRc(null, consoleDummy);
        else
            for (String file : rcFiles)
                commands.loadRc(file, consoleDummy);
        
        for (String file : jsonFiles)
            loadJson(file);
    }

    /** Separate method so that subclasses can fully disable it */
    protected synchronized void addShutdownHook() {
        if (shutdownHookAdded)
            return;
        shutdownHookAdded = true;
        Runtime.getRuntime().addShutdownHook(new Thread(this::handleShutdown));
    }

    /**
     * Shutdown hook to salvage state when the console user accidentally presses Ctrl-C.
     * If the state hasn't been saved within SAVE_GRACE, saves it to a temporary file and prints
     * the name of this file.
     */
    protected synchronized void handleShutdown() {
        if (System.currentTimeMillis() - lastSave < SAVE_GRACE)
            return;
        try {
            File file = File.createTempFile("room", ".json");
            try (PrintWriter writer = new PrintWriter(file)) {
                writer.println(killAndSave());
            }
            System.out.println("If you exited by accident, you can restore the room state by passing:");
            System.out.println("--json " + file.getAbsolutePath());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    public void run() {
        if (consoleDummy != null)
            return;
        beforeRun();
        setUp();
        addShutdownHook();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.err.print("> ");
            try {
                String cmd = in.readLine();
                if (cmd.startsWith("/")) {
                    command(consoleDummy, cmd);
                } else {
                    commands.adminCommand(cmd, consoleDummy);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected boolean shouldSaveUser(MultiUser user) {
        return user.isConnected();
    }
    
    private static final JsonSerializer<Multi> serializer = 
            new JsonSerializer<Multi>(Multi.class) {
        @Override
        void customSave(Multi t, Map<String, Object> map) {
            if (t == null) return;
            t.pulseFreq = -1;
            t.murder = true;
            t.autoJoin = false;
            map.put("historySize", t.getHistorySize());
            Map<String, JsonValue> usersToSave = new LinkedHashMap<>();
            synchronized (t.users) {
                Function<MultiUser, String> getNumber = u -> String.valueOf(u.getNumber());
                map.put("idSize", t.ids.length);
                map.put("kickVotes", JsonValue.wrapMappings(t.kickVotes, getNumber));
                map.put("ignored", JsonValue.wrapMappings(t.ignored, getNumber));
                map.put("admins", t.users.stream().filter(t::isAdmin).map(getNumber)
                        .toArray(String[]::new));
                t.users.removeIf(user -> {
                    if (t.shouldSaveUser(user))
                        usersToSave.put(getNumber.apply(user), user.killAndSave());
                    else
                        user.schedSendDisconnect();
                    t.ids[user.getNumber()] = null;
                    t.removeCommon(user);
                    return true;
                });
                map.put("users", usersToSave);
                map.put("consoleDummy", t.consoleDummy.killAndSave());
            }
            map.put("proxies", t.proxies.save());
            map.put("multiUserStatic", MultiUser.saveStatic());
        }
        @Override
        void customLoad(Multi t, Map<String, JsonValue> map) {
            if (t == null) return;
            JsonValue.doIfPresent(t::migrateDefaultTopics, map.get("multiUserStatic"));
            JsonValue.doIfPresent(MultiUser::loadStatic, map.get("multiUserStatic"));
            JsonValue.doIfPresent(t.proxies::load, map.get("proxies"));
            JsonValue.doIfPresent(size -> t.setHistorySize(size.getInt()), map.get("historySize"));
            JsonValue.doIfPresent(t.consoleDummy::load, map.get("consoleDummy"));
            
            JsonValue idSize = map.get("idSize"),
                users = map.get("users"),
                admins = map.get("admins"),
                ignored = map.get("ignored"),
                kickVotes = map.get("kickVotes");
            synchronized (t.users) {
                if (!JsonValue.isNull(idSize))
                    t.ids = Arrays.copyOf(t.ids, Math.max(t.ids.length, idSize.getInt()));
                if (!JsonValue.isNull(users)) {
                    users.getMap().forEach((k, v) -> {
                        MultiUser user = t.makeUser();
                        user.setProxy(t.proxies.get(
                                v.getMap().get("proxy").getAs(String[].class)));
                        if (user.getProxy() == null)
                            user.setProxy(t.proxies.get("0", 0));
                        user.load(v);
                        t.ids[user.getNumber()] = user;
                        t.users.add(user);
                    });
                }
                if (!JsonValue.isNull(admins))
                    for (JsonValue id : admins.getList())
                        t.ids[id.getInt()].setAdminToken(t.adminToken);
                if (!JsonValue.isNull(ignored))
                    t.ignored.putAll(ignored.unwrapMappings(t::userFromName, 
                            WeakHashMap<MultiUser, Set<MultiUser>>::new, HashSet<MultiUser>::new));
                if (!JsonValue.isNull(kickVotes))
                    t.kickVotes.putAll(kickVotes.unwrapMappings(t::userFromName, 
                            WeakHashMap<MultiUser, Set<MultiUser>>::new, HashSet<MultiUser>::new));
                t.users.forEach(MultiUser::start);
                if (t.pulseFreq > 0)
                    t.lastPulse = 0;
                if (t.moveProxies)
                    t.tellAdmin("You have enabled proxymove, which is likely to make the room unjoinable. Use /!noproxymove to disable it.");
            }
            System.gc();
        }
    };

    /**
     * Default topics used to be stored in UserConnection; this moves them into Multi.
     * Also, since JoinType.BARE was separated from TEXT at the same time, we add it to toothpasteTypes
     * here if TEXT is present.
     */
    protected void migrateDefaultTopics(JsonValue multiUserStatic) {
        JsonValue.doIfPresent(topics -> defaultTopics = topics,
                multiUserStatic.getMap().get("defaultTopicsArray"),
                String[].class);
        if (toothpasteTypes.contains(JoinType.TEXT))
            toothpasteTypes.add(JoinType.BARE);
    }
    
    /** Serializes the room state, removes all users and stops looking for users. The state is returned. */
    protected synchronized JsonValue killAndSave() {
        lastSave = System.currentTimeMillis();
        tellAdmin("Saving room state...");
        JsonValue result = serializer.save(this);
        tellAdmin("Room state saved");
        return result;
    }
    
    /** Serializes the room state, removes all users and stops looking for users. The state is saved to the given file. */
    public void killAndSave(String file) {
        try (PrintWriter writer = new PrintWriter(file)) {
            writer.println(killAndSave());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    /** Loads the serialized room state. */
    protected synchronized Multi load(JsonValue spec) {
        tellAdmin("Restoring room state...");
        serializer.load(this, spec);
        tellAdmin("Room state restored");
        commands.adminCommand("list", consoleDummy);
        commands.adminCommand("proxies", consoleDummy);
        return this;
    }
    
    /** Loads the serialized room state from the given file. */
    protected void loadJson(String file) {
        try {
            String json;
            try (Scanner s = new Scanner(new File(file));) {
                json = s.useDelimiter(Util.EOF).next();
            }
            load(JsonParser.parse(json));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    /** Rudimentary parsing of command line options. Each option has one argument. */
    protected void parseArgs(String[] args) {
        for (int i = 1; i < args.length; i += 2)
            parseArg(args[i - 1], args[i]);
    }
    
    protected void parseArg(String k, String v) {
        if ("--rc".equals(k))
            rcFiles.add(v);
        else if ("--json".equals(k))
            jsonFiles.add(v);
    }

    private static Multi m;

    public static void main(String[] args) {
        m = new Multi();
        m.parseArgs(args);
        m.run();
    }

}
